FROM node:20

WORKDIR be

COPY package*.json ./

RUN npm install

# For production
# RUN npm ci --only=production

COPY . .

EXPOSE 8080

CMD [ "npm", "start" ]
